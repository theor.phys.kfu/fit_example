#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <random>
#include <boost/math/constants/constants.hpp>
#include <specmath/minsearch.h>
#include <specmath/grid.h>


constexpr double pi = boost::math::double_constants::pi;
constexpr size_t Sample_Size = 1000;

int main()
{
    specmath::grid1d<double> grid(0.0, 2*pi, Sample_Size);

    std::random_device rd;
    std::mt19937 gen(rd());
    const double dispersion = 0.2;
    std::normal_distribution<double> dist(0.0, dispersion);

    const double A = 1.0; // Amplitude
    const double omega = 1.0; // frequency
    const double phi = pi/6; // phase

    grid.apply([&](double x)
    {
        // generate a random data
       return A * sin(omega * x + phi) + dist(gen);
    });


    specmath::minsearch<double> minsearch(1.0, 10);

    std::vector<double> x = {0.0, 0.0, 0.0}; // init point

    double sqr_sigma = minsearch.find_minimum([&](const std::vector<double> & v)
    {
        auto it = grid.first_point(); // x = 0.0, .. 2*pi
        double res = 0.0;

        double _A  = v[0];
        double _omega = v[1];
        double _phi = v[2];

        for (const auto & val : grid)
        {
            double xi = *(it++);
            double yi = val;

            double d = _A * sin(_omega * xi + _phi) - yi;

            res += d*d;
        }

        return res;

    }, x, specmath::breaker<double>(1e-6));

    std::cout << "Amplitude = " << x[0] <<  std::endl;
    std::cout << "frequency = " << x[1] << std::endl;
    std::cout << "phase = " << x[2] << std::endl;

    std::cout << "sqr_sigma = " << sqr_sigma << std::endl;

    {
        std::ofstream out("data.txt");
        grid.serialize(out);
    }

    grid.parallel_apply([&](const double & xi)
    {
        return x[0]*sin(x[1]*xi + x[2]);
    });

    {
        std::ofstream out("fit_data.txt");
        grid.serialize(out);
    }

    return 0;
}
