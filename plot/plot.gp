set terminal pngcairo  enhanced font "arial, 20" fontscale 1.0 size 800, 600
set output 'fig.png'

#set grid polar 1.047198

set xrange [ * : * ] noreverse writeback
set yrange [ * : * ] noreverse writeback

set xzeroaxis lt 1 lw 1 lc 'black'

set xlabel "x"

set ylabel "y"

set grid

plot 'data.txt' u 1:2 w lp lw 2 ti "experiment",\
'fit_data.txt' u 1:2 w l lw 3 lc 'black' ti 'fit data'

